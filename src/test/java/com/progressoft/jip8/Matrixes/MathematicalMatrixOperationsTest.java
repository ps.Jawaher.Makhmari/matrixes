package com.progressoft.jip8.Matrixes;

import com.progressoft.jip8.Matrixes.Matrix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MathematicalMatrixOperationsTest {


    @Test
    public void givenTwoValidMatrix_whenConstructing_thenComputeSum() {
        Matrix operations = new Matrix();
        int matrix1[][] = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };

        int matrix2[][] = {{16, 15, 14, 13},
                {12, 11, 10, 9},
                {8, 7, 6, 5},
                {4, 3, 2, 1}
        };


//below for loop for matrix 1 iterator
        System.out.println("Matrix1: ");
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                //for print matrix one
                System.out.print(matrix1[i][j] + "\t");
            }
        }
         //below for loop for matrix 2 iterator
          System.out.println("\nMatrix2: ");
        for (int i = 0; i < matrix2.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                 System.out.print(matrix2[i][j] + "\t");
            }
        }
        // calculate sum of matrix1 and matrix2
        int sum[][] = operations.sum(matrix1, matrix2);
        for (int i = 0; i < sum.length; i++) {
            for (int j = 0; j < sum[i].length; j++) {
                System.out.print(sum[i][j] + "\t");
            }
        }

    }
    @Test
            public  void givenTwoValidMatrix_whenConstructing_thenDoScale() {
        Matrix operations = new Matrix();
        int matrix1[][] = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };

        int matrix2[][] = {{16, 15, 14, 13},
                {12, 11, 10, 9},
                {8, 7, 6, 5},
                {4, 3, 2, 1}
        };
        operations.scale(matrix1, 3);
    }


   @Test
           public  void givenTwoValidMatrix_whenConstructing_thenDoTranspose() {
       Matrix operations = new Matrix();
       int matrix1[][] = {{1, 2, 3, 4},
               {5, 6, 7, 8},
               {9, 10, 11, 12},
               {13, 14, 15, 16}
       };

       int matrix2[][] = {{16, 15, 14, 13},
               {12, 11, 10, 9},
               {8, 7, 6, 5},
               {4, 3, 2, 1}
       };
       operations.transpose(matrix1);
   }

     @Test
             public  void givenTwoValidMatrix_whenConstructing_thenDoMultiplication() {
         Matrix operations = new Matrix();
         int matrix1[][] = {{1, 2, 3, 4},
                 {5, 6, 7, 8},
                 {9, 10, 11, 12},
                 {13, 14, 15, 16}
         };

         int matrix2[][] = {{16, 15, 14, 13},
                 {12, 11, 10, 9},
                 {8, 7, 6, 5},
                 {4, 3, 2, 1}
         };
         operations.matrixMultiplication(matrix1, matrix2);
     }

     @Test
             public  void givenTwoValidMatrix_whenConstructing_thenRemoveSecondRowThirdColumn() {
         //5.SubMatrix after removing second row third column
         Matrix operations = new Matrix();
         int matrix1[][] = {{1, 2, 3, 4},
                 {5, 6, 7, 8},
                 {9, 10, 11, 12},
                 {13, 14, 15, 16}
         };

         int matrix2[][] = {{16, 15, 14, 13},
                 {12, 11, 10, 9},
                 {8, 7, 6, 5},
                 {4, 3, 2, 1}
         };
         operations.subMatrix(matrix1, 2, 3);
     }

     @Test
             public  void givenTwoValidMatrix_whenConstructing_thenSquareMatrix() {
         //6.Square Matrix operations
         System.out.println("Matrix1 Square Operations: ");
         Matrix operations = new Matrix();
         int matrix1[][] = {{1, 2, 3, 4},
                 {5, 6, 7, 8},
                 {9, 10, 11, 12},
                 {13, 14, 15, 16}
         };

         int matrix2[][] = {{16, 15, 14, 13},
                 {12, 11, 10, 9},
                 {8, 7, 6, 5},
                 {4, 3, 2, 1}
         };
         operations.SquareMatrixOprations(matrix1);
     }

     @Test
             public  void givenTwoValidMatrix_whenConstructing_thenDeterminateMatrix(){
        //7.Determinant of matrix1
         Matrix operations = new Matrix();
         int matrix1[][] = {{1, 2, 3, 4},
                 {5, 6, 7, 8},
                 {9, 10, 11, 12},
                 {13, 14, 15, 16}
         };

         int matrix2[][] = {{16, 15, 14, 13},
                 {12, 11, 10, 9},
                 {8, 7, 6, 5},
                 {4, 3, 2, 1}
         };
        System.out.println("Determinant of matrix1: "
                + operations.determinent(matrix1));

    }

}